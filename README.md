# *zoe*, a library providing an API for developing simple text-based environments.
***
**zoe** is is a programming library providing a simple application programming interface (API) that allows the programmer to write text-based user interfaces in a terminal-independent manner.

When **zoe** is initialized, the application specifies the number of rows and columns needed for the dialogs. The rest of the lines of the terminal will be used as 'console' for the display of the application (printf, etc.). If no line is available then these displays will be in the dialog section; it will then be the application to manage the location of the cursor to avoid overlap between dialogs and displays.

The dialog forms handled by zoe are:
   - *menu*: A menu consists of a list of answers in which a user makes a choice to answer a question (explicit or implicit).
   - *open question*: An open question is a question for which there are no pre-established answers given to the user, so the user is completely free in his answer.
   - *closed question*: A closed question is a question for which the user is offered a choice among pre-established answers. These pre-established answers are presented in the form of a menu.
   - *confirmation box*: As its name suggests, it allows to validate or cancel a decision.
   - *list of items*: It consists of a list of answers in which a user makes one or more choice to answer a question.
   - *file and directory chooser*: This is the list of existing files and directories in a specified directory that can be selected by the user. He can browse this list and go down / up in the different directories.
   - *progress bar box*: A progress bar can be used to show a user how far along he is in a process.
   - *message* and *information boxes*: A message and infirmation boxes allow the application to give information to the user.
   - *error box*: An error box allows the application to report an error to the user.
   - *text* and *title*.

At each interaction with the user, an online help is displayed using the **CTRL A** keys. Minimal help on the keys available to interact is provided by **** and supplemented by the application for any information specific to it.

## LICENSE
**zoe** is covered by the GNU General Public License (GPL) version 3 and above.
## EXAMPLES

**Note:** Screenshots with file choosers have not been updated. Since version 1.4.0, this implementation has been redesigned.

An example with a remanent menu and a closed question with its list of possible answers, plus some additional texts.
![zoe](README_images/zoe_01.png  "Example #1")

Resumption of the previous example with a new closed question. The answer to the first question is displayed.
![zoe](README_images/zoe_02.png  "Example #2")

Resumption of the previous example with a new open question and its response field (here 250 characters maximum requested by the application). Help dedicated to this issue (required with ctrl A as specified in the title field) is also displayed.
![zoe](README_images/zoe_03.png  "Example #3")

Resumption of the previous example with a question in the form of a list of items with checkboxes.
![zoe](README_images/zoe_04.png  "Example #4")

Resumption again of the previous example with directory and a file chooser forms.
![zoe](README_images/zoe_05.png  "Example #5")

Resumption again (and again) of the previous example with a progress bar form.
![zoe](README_images/zoe_06.png  "Example #6")

Resume the previous example with a long information message from the application.
![zoe](README_images/zoe_07.png  "Example #7")

There would be plenty of others to show but it's up to you to try ....
## API DEFINITION
### zoe_init
*Declaration:*

```C
#include "zoe.h"
void zoe_init( int lines_needed, int columns_needed, zoe_log log );
```

*Description:*

**zoe_init** is normally the first **zoe** function to call when initializing a program. The **zoe_init** code determines the terminal type and initializes all **zoe** data structures. If errors occur, **zoe_init** writes an appropriate error message to standard error and exits. It also specifies whether a user display area ("Log" accessible via stdout / stder) is necessary or not.

**lines_needed** is the minimum number of lines needed to display the dialogue part.

**columns_needed** is the minimum number of columns needed to display the dialogue part.

**log** is an enumeration (WITH_LOG, WITHOUT_LOG) about the user display area setting.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

int main() {
	zoe_init(40, 100, WITH_LOG);
	...
	return 0;
}
```

### zoe_end
*Declaration:*

```C
#include "zoe.h"
void zoe_end( void )
```

*Description:*

A program should always call **zoe_end** before exiting from **zoe** mode. This function restores tty modes and resets the terminal into the proper non-visual mode.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

int main() {
	zoe_init(40, 100, WITH_LOG);
	...
	zoe_end();
	return 0;
}
```

### zoe_setattributes
*Declaration:*

```C
#include "zoe.h"
void zoe_setattributes(short objects, zoe_color color, zoe_bold character);
```

*Description:*

The **zoe_setattributes** function allows to define the characteristics of the forms handled by **zoe**, ie the color and the appearance (bold or not).

**objects** is the result of ORing all constant defining handled forms: *ZOE_OBJECT_TITLE*, *ZOE_OBJECT_QUESTION*, *ZOE_OBJECT_ANSWER*, *ZOE_OBJECT_MENU*, *ZOE_OBJECT_GONOGO*, *ZOE_OBJECT_LIST*, *ZOE_OBJECT_CHOOSER*, *ZOE_OBJECT_PROGRESSBAR*, *ZOE_OBJECT_BAR*, *ZOE_OBJECT_TEXT*, *ZOE_OBJECT_ERROR*, *ZOE_OBJECT_MESSAGE* and *ZOE_OBJECT_INFORMATION*.

The different values for **color** are _black_, _red_, _green_, _yellow_, _blue_, _magenta_, _cyan_ and _white_.

The different values for **character** are _boldoff_ and _boldon_;

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

int main() {
	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	...
	zoe_end();
	return 0;
}
```

### zoe_title
*Declaration:*

```C
#include "zoe.h"
void zoe_title( const char * title);
```

*Description:*

The **zoe_title** function allows you to specify the text that will appear on the left of the first line of the dialogue part, separated from the rest by a horizontal line.

**title** is the text to display.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

int main() {
	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");
	...
	zoe_end();
	return 0;
}
```

### zoe_menu
*Declaration:*

```C
#include "zoe.h"
int  zoe_menu( objectID * objId, int line, int column, const char ** values, size_t nvalues, const char * title);
```

*Description:*

The **zoe_menu** function displays a menu at the specified coordinates. A menu is a collection of items from which one can be chosen.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the menu will be displayed. **column** is the column number of the left corner of the menu. 

**values** is an array of character strings specifying each line of the menu.

**nvalues** is the number of lines of **values**.

**title** is a character string specify the title of the menu. If **title** is empty, no title will be display.

*Return value:*

If successful, the selection number is returned (null or positive number), otherwise a negative number is returned:
   - **ZOE_ABORT** (-1): Session aborted by user.
   - **ZOE_BACK** (-2): Come back to previous dialog.

*Example:*

```C
...
#include "zoe.h"

int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id;
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```


### zoe_text
*Declaration:*

```C
#include "zoe.h"
void zoe_text( objectID * objId, int line, int column, const char * text);
```

*Description:*

The **zoe_text** function displays a text at the specified coordinates.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the text will be displayed. **column** is the column number of the first character of the text. 

**text** is a character string to display. 

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_close_question
*Declaration:*

```C
#include "zoe.h"
int zoe_close_question( objectID * objId, int line, int column, const char * question, const char ** values, size_t nvalues);
```

*Description:*

The **zoe_close_question** function displays a question at the specified coordinates and provides predefined and possible answers to the user as a menu.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the question will be displayed. **column** is the column number of the first character of the question. 

**question** is the character string to display. 

**values** is an array of character strings specifying the possible answers.

**nvalues** is the number of lines of **values**.

*Return value:*

If successful, the selection number is returned (null or positive number), otherwise a negative number is returned:
   - **ZOE_ABORT** (-1): Session aborted by user.
   - **ZOE_BACK** (-2): Come back to previous dialog.
 
*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_open_question
*Declaration:*

```C
#include "zoe.h"
int zoe_open_question( objectID * objId, int line, int column, const char * question, const char * help, char * answer, size_t lengthanwser);
```

*Description:*

The **zoe_open_question** function displays a question at the specified coordinates as well as the answer box.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the question will be displayed. **column** is the column number of the first character of the question. 

**question** is the character string to display.

**help** is a character string that will be displayed on help request.

**answer** is the pointer to the character string where the answer is stored.

**lengthanwser** is the size of **answer**.

*Return value:*

If successful (**ZOE_OK**), the result in the dedicated parameter, otherwise a negative number is returned:
   - **ZOE_ABORT** (-1): Session aborted by user.
   - **ZOE_BACK** (-2): Come back to previous dialog.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                   "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                   "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                   "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                   "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_list
*Declaration:*

```C
#include "zoe.h"
int zoe_list( objectID * objId, int line, int column, const char * question, struct zoe_list_item values[], size_t nvalues);
```

*Description:*

The **zoe_list** function displays, at the specified coordinates, a list of items to be selected or deselected by the user.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the question will be displayed. **column** is the column number of the first character of the question. 

**question** is the character string to display.

**values** is an array of *zoe_list_item* specifying each line of the list.

**nvalues** is the number of lines of **values**.

*Return value:*

If successful (**ZOE_OK**), the result in the dedicated parameter, otherwise a negative number is returned:
   - **ZOE_ABORT** (-1): Session aborted by user.
   - **ZOE_BACK** (-2): Come back to previous dialog.

*Example:*

```C
#...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                   "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                   "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                   "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                   "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_file_chooser
*Declaration:*

```C
#include "zoe.h"
int zoe_file_chooser( objectID * objId, int line, int column, const char * question,  const char *path, zoe_choose type, const  char * suffix[], char * answer );
```

*Description:*


The **zoe_file_chooser** function displays ...........

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**line** is the line number of the dialog section (starting at 1, after the title field), from which the question will be displayed. **column** is the column number of the first character of the question. 

**question** is the character string to display.

**path** is the root of directories and files to list.

**type** (= *CHOOSE_DIR* or *CHOOSE_FILE*) indicates if the use has to choose a directory or a file.

**suffix** is an array of character strings listing the suffixes of the selectable files. 

**answer** is the pointer to the character string where the answer is stored.

*Return value:*

If successful (**ZOE_OK**), the result in the dedicated parameter, otherwise a negative number is returned:
   - **ZOE_ABORT** (-1): Session aborted by user.
   - **ZOE_BACK** (-2): Come back to previous dialog.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                   "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                   "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                   "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                   "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_progressbar and zoe_progressbar_display

*Declaration:*

```C
#include "zoe.h"
void zoe_progressbar( objectID *id,  const char * message );
void zoe_progressbar_display( objectID id, int percent);
```
*Description:*

The **zoe_progressbar** function displays a message with a progress bar, in the center of the window. This form is to be cleared by the application. The **zoe_progressbar_display** sets the state of the bar to reflect the progress. 

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**message** is the character string to display.

**percent** is the new state of the bar.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                   "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                   "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                   "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                   "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_gonogo
*Declaration:*

```C
#include "zoe.h"
boolean	zoe_gonogo(objectID * objId,  const char * question,  const char * go,  const char * nogo );
```

*Description:*

The **zoe_gonogo** function displays a confirmation box in the center of the window.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**question** is the character string to display.

**go** is the label, character string, of the right "button".

**nogo** is the label, character string, of the left "button".

*Return value:*
   - **TRUE**; First proposal selected.
   - **FALSE**: Second proposal selected.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                   "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                   "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                   "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                   "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", help, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);
Q8:
	b = zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No");
	fprintf(stdout, "Answer is.....%d\n", b);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_message
*Declaration:*

```C
#include "zoe.h"
void zoe_message( const char * message );
```

*Description:*

The **zoe_message** function displays a message, to be validated by the user, in the center of the window.

**message** is the character string to display.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *anytext = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                      "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                      "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                      "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                      "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", anytext, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);
Q8:
	b = zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No");
	fprintf(stdout, "Answer is.....%d\n", b);
Q9:
	zoe_message(anytext);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_information
*Declaration:*

```C
#include "zoe.h"
void zoe_information( objectID * objId,  const char * label );
```

*Description:*

The **zoe_information** function displays a message, in the center of the window. This form is to be cleared by the application.

**objId**: This is the pointer to an _objectID_ where the resulting form identification is stored.

**label** is the character string to display.

*Return value:*
None.

*Example:*

```C
...
#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
...
int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *anytext = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                      "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                      "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                      "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                      "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
	char buf[64];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", anytext, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);
Q8:
	b = zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No");
	fprintf(stdout, "Answer is: %d\n", b);
Q9:
	zoe_message(anytext);
Q10:
	for (i = 5; i > 0; i--) {
		sprintf(buf, "The result will appear in %d seconds ....", i);
		zoe_information(&id, buf);
		sleep(1);
		zoe_clearObject(id);
	}
	zoe_message(anytext);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_error
*Declaration:*

```C
#include "zoe.h"
void zoe_error(  const char * message );
```

*Description:*

The **zoe_error** function displays a error message, to be validated by the user, in the center of the window.

**message** is the character string to display.

*Return value:*
None.

*Example:*

```C
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#include "zoe.h"

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;

int main() {
	const char *features[] = { "First feature", "Second feature", "Third feature", "Fourth feature",
	                           "Fifth feature", "Sixth feature", "Seventh feature", "Eighth feature",
	                           "Ninth feature", "Tenth feature", "Eleventh feature", "Twelfth feature",
	                           "Thirteenth feature", "Fourteenth feature", "Fifteenth feature" };
	const char *colors[] = { "black", "red", "green", "yellow", "blue",
	                         "magenta", "cyan", "white", "normal" };
	const char *text1 = "HEADER 1:";
	const char *text2 = "HEADER 2:";
	const char *anytext = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod " \
	                      "tempor invidunt ut labore et dolor magna aliquyam-erat, sed diam voluptua. " \
	                      "Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore" \
	                      "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " \
	                      "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit.";
	const struct zoe_list_item list[] = {
		{ FALSE, "bold" },
		{ TRUE, "boldunderline" },
		{ FALSE, "underline" },
		{ TRUE, "italic" },
		{ FALSE, "normal"}
	};
	const char *suffix[] = { "o", "a", "\0"};

	zoe_init(40, 100, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT |  ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_title("THIS IS A TITLE");

	int i;
	objectID id, id1, id2;
	boolean b;
	char answer[251];
	char buf[64];
Q0:
	i = zoe_menu(&id, 1, 1, features, 15, "MAIN MENU");
	if (i < 0) goto ABORT;
	fprintf(stdout, "Answer is.....%s\n", features[i]);
	zoe_text(&id1, 1, 29, text1);
	zoe_text(&id2, 3, 29, text2);
Q1:
	i = zoe_close_question(&id, 1, 39, "Your favorite color?", colors, 9);
	if (i == ZOE_BACK) {
		zoe_clearObject(id1);
		zoe_clearObject(id2);
	}
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is.....%s\n", colors[i]);
Q3:
	i = zoe_open_question(&id, 3, 39, "Another question?   ", anytext, answer, 250);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is.....%s\n", answer);
Q4:
	fprintf(stdout, "Proposed List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
	i = zoe_list(&id, 10, 29, "What attributes do you want?", (struct zoe_list_item *) list, 5);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Requested List: ");
	for (i = 0; i < 5; i++) if (list[i].requested) fprintf(stdout, "%s ", list[i].label);
	fprintf(stdout, "\n");
Q5:
	i = zoe_file_chooser(&id, 10, 62, "Choose a directory....", ".", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q4)
	fprintf(stdout, "Answer is: %s\n", answer);
Q6:
	i = zoe_file_chooser(&id, 10, 88, "Choose a file....", "./linux", CHOOSE_FILE, suffix, answer);
	ZOE_REPORT(i, ABORT, Q5)
	fprintf(stdout, "Answer is: %s\n", answer);
Q7:
	zoe_progressbar(&id, message);
	for (int i = 1; i <= 10; i++) {
		zoe_progressbar_display(id, i * 10);
		sleep(1);
	}
	zoe_clearObject(id);
Q8:
	b = zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No");
	fprintf(stdout, "Answer is: %d\n", b);
Q9:
	zoe_message(anytext);
Q10:
	for (i = 5; i > 0; i--) {
		sprintf(buf, "The result will appear in %d seconds ....", i);
		zoe_information(&id, buf);
		sleep(1);
		zoe_clearObject(id);
	}
	zoe_message(anytext);
	zoe_error(anytext);
	zoe_error("Another error, but shorter....");
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
```

### zoe_getnextfreeline
*Declaration:*

```C
#include "zoe.h"
void zoe_getnextfreeline( void );
```

*Description:*
The **zoe_getnextfreeline** function returns the next  free line number.

*Return value:*
Next free line number.

*Example:*

```C
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <termcap.h>
#include <stdarg.h>
#include <sys/ioctl.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe.h"
//------------------------------------------------------------------------------
// MACRO_CODES
//------------------------------------------------------------------------------
#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main() {
	const char *colors[] = { "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white", "normal" };
	const char *attribut[] = { "bold", "boldunderline", "underline", "italic", "normal" };
	const char *help = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                   "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                   "diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem " \
	                   "ipsum dolor sit X.";
	const char *error = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                    "aliquyam erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                    "diam voluptua.";
	const char *message = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna " \
	                      "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                      "diam voluptua.\n" \
	                      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolor magna" \
	                      "aliquyam-erat, sed diam voluptua. Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " \
	                      "diam voluptua.";
	const char *suffix[] = { "o", "a", "\0"};
	//--------------------------------------------------------------------------
	zoe_init(20, 50);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT | ZOE_OBJECT_MESSAGE, white, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_CHOOSER | ZOE_OBJECT_PROGRESSBAR, white, boldon);
	zoe_setattributes(ZOE_OBJECT_MENU | ZOE_OBJECT_GONOGO | ZOE_OBJECT_LIST, yellow, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR | ZOE_OBJECT_BAR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_INFORMATION, green, boldon);
	zoe_setattributes(ZOE_OBJECT_ANSWER, blue, boldon);

	zoe_title("THIS IS A TITLE");

	int i;
	objectID id;
	boolean b;
	char answer[251];

Q0:
	i = zoe_open_question(&id, zoe_getnextfreeline(), 1, "A question?         ", help, answer, 20);
	ZOE_REPORT(i, ABORT, ABORT)
	fprintf(stdout, "Answer is: %s\n", answer);
Q1:
	i = zoe_close_question(&id, zoe_getnextfreeline(), 1, "Your favorite color?", colors, 9);
	ZOE_REPORT(i, ABORT, Q0)
	fprintf(stdout, "Answer is: %s\n", colors[i]);
Q2:
	i = zoe_close_question(&id, zoe_getnextfreeline(),1, "Which attribut?     ",  attribut, 5);
	ZOE_REPORT(i, ABORT, Q1)
	fprintf(stdout, "Answer is: %s\n", attribut[i]);
Q3:
	i = zoe_file_chooser(&id, zoe_getnextfreeline(), 1, "Choose a directory: ", "/", CHOOSE_DIR, NULL, answer);
	ZOE_REPORT(i, ABORT, Q2)
	fprintf(stdout, "Answer is: %s\n", answer);
Q4:
	i = zoe_file_chooser(&id, zoe_getnextfreeline(), 1, "Choose a file:      ", "../..", CHOOSE_FILE, NULL, answer);
	ZOE_REPORT(i, ABORT, Q3)
	fprintf(stdout, "Answer is: %s\n", answer);
Q5:
	b = zoe_gonogo(&id, "Go on next?", "Yes", "No");
	fprintf(stdout, "Answer is: %d\n", b);
END:
	zoe_end();
	return 0;
ABORT:
	fprintf(stdout, "\nAbort...\n\n");
	goto END;
}
//==============================================================================

```

### zoe_clearObject
*Declaration:*

```C
#include "zoe.h"
void zoe_clearObject( objectID objId );
```

*Description:*

The **zoe_clearObject** function clears the form  whose identification is passed as parameter.

**objId**: This is the identification of the form to clear.

*Return value:*
None.

*Example:*
Refer to sections Q1 and Q9 in above examples.

## STRUCTURE OF THE APPLICATION
This section walks you through **zoe**'s structure. Once you understand this structure, you will easily find your way around in **zoe**'s code base.

``` bash
$ yaTree
./                           # Application level
├── README_images/           # Screenshots for documentation
│   ├── zoe_01.png           # 
│   ├── zoe_02.png           # 
│   ├── zoe_03.png           # 
│   ├── zoe_04.png           # 
│   ├── zoe_05.png           # 
│   ├── zoe_06.png           # 
│   └── zoe_07.png           # 
├── examples/                # Example directory
│   ├── Makefile             # Makefile
│   └── basic.c              # Example of 'zoe' usage
├── src/                     # Source directory
│   ├── Makefile             # Makefile
│   ├── zoe.h                # Application headerfile
│   ├── zoe_close_question.c # Close Question form
│   ├── zoe_data.h           # Internal global header file
│   ├── zoe_file_chooser.c   # File chooser form
│   ├── zoe_freeline.c       # Next free line
│   ├── zoe_funcs.c          # Various useful functions
│   ├── zoe_gonogo.c         # Go-Nogo form
│   ├── zoe_help.c           # Help management
│   ├── zoe_help.h           # Help for widgets
│   ├── zoe_information.c    # Information form
│   ├── zoe_init_end.c       # Init and End session
│   ├── zoe_list.c           # List form
│   ├── zoe_mapping.c        # Object screen location functions
│   ├── zoe_menu.c           # Menu form
│   ├── zoe_objectID.c       # Object identification management
│   ├── zoe_open_question.c  # Open Question form
│   ├── zoe_progressbar.c    # Progress bar form
│   ├── zoe_screenmap.c      # Screen mapping function
│   ├── zoe_setattributes.c  # Object attributes management
│   ├── zoe_text.c           # Text form
│   └── zoe_title.c          # Title form
├── COPYING.md               # GNU General Public License markdown file
├── LICENSE.md               # License markdown file
├── Makefile                 # Makefile
├── README.md                # ReadMe Markdown file
├── RELEASENOTES.md          # Release Notes Mark-Down file
└── VERSION                  # Version identification text file

3 directories, 37 files
$ 

```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE
```Shell
$ cd libZoe
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libZoe
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## SOFTWARE REQUIREMENTS
- For usage:
    - *libtinfo* library:terminal management library.
- For development:
    - *GengetOpt* binary package installed, version 2.22.6. for code examples.
- For the example presented above:
   - Save it to a file, say _zoe/demo.c_.
   - Build the **zoe** library as explained above.
   - Compile the example (_src_ is the dirctory where the header file **zoe.h** can be found. _libtinfo_ is the library for terminal management) and run it:
   
```Shell
		$ gcc -I src demo.c ./linux/zoe.a -ltinfo
		$ ./a.out
```

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***