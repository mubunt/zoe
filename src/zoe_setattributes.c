//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_setattributes( short dest, zoe_color color, zoe_bold bold) {
	if (CHECK_BIT(dest, 0)) {
		attributs.title.color = FOREGROUND(color);
		attributs.title.bold = bold;
	}
	if (CHECK_BIT(dest, 1)) {
		attributs.question.color = FOREGROUND(color);
		attributs.question.bold = bold;
	}
	if (CHECK_BIT(dest, 2)) {
		attributs.menu.color = FOREGROUND(color);
		attributs.menu.bold = bold;
	}
	if (CHECK_BIT(dest, 3)) {
		attributs.gonogo.color = FOREGROUND(color);
		attributs.gonogo.bold = bold;
	}
	if (CHECK_BIT(dest, 4)) {
		attributs.list.color = FOREGROUND(color);
		attributs.list.bold = bold;
	}
	if (CHECK_BIT(dest, 5)) {
		attributs.chooser.color = FOREGROUND(color);
		attributs.chooser.bold = bold;
	}
	if (CHECK_BIT(dest, 6)) {
		attributs.text.color = FOREGROUND(color);
		attributs.text.bold = bold;
	}
	if (CHECK_BIT(dest, 7)) {
		attributs.error.color = FOREGROUND(color);
		attributs.error.bold = bold;
	}
	if (CHECK_BIT(dest, 8)) {
		attributs.message.color = FOREGROUND(color);
		attributs.message.bold = bold;
	}
	if (CHECK_BIT(dest, 9)) {
		attributs.information.color = FOREGROUND(color);
		attributs.information.bold = bold;
	}
	if (CHECK_BIT(dest, 10)) {
		attributs.progressbar.color = FOREGROUND(color);
		attributs.progressbar.bold = bold;
	}
	if (CHECK_BIT(dest, 11)) {
		attributs.bar.color = BACKGROUND(color);
		attributs.bar.bold = bold;
	}
	if (CHECK_BIT(dest, 12)) {
		attributs.answer.color = FOREGROUND(color);
		attributs.answer.bold = bold;
	}
}
//==============================================================================
