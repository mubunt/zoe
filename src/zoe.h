//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------
#ifndef ZOE_H
#define ZOE_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define ZOE_OK					0
#define ZOE_ABORT				-1
#define ZOE_BACK				-2
#define ZOE_GO					TRUE
#define ZOE_NOGO				FALSE

#define ZOE_OBJECT_TITLE		0x0001
#define ZOE_OBJECT_QUESTION		0x0002
#define ZOE_OBJECT_MENU			0X0004
#define ZOE_OBJECT_GONOGO		0X0008
#define ZOE_OBJECT_LIST			0X0010
#define ZOE_OBJECT_CHOOSER		0x0020
#define ZOE_OBJECT_TEXT			0X0040
#define ZOE_OBJECT_ERROR		0X0080
#define ZOE_OBJECT_MESSAGE		0X0100
#define ZOE_OBJECT_INFORMATION	0X0200
#define ZOE_OBJECT_PROGRESSBAR	0X0400
#define ZOE_OBJECT_BAR			0X0800
#define ZOE_OBJECT_ANSWER		0X1000
//--------------------|------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { FALSE = 0, TRUE = 1 } boolean;
typedef enum { black = 0, red, green, yellow, blue, magenta, cyan, white } zoe_color;
typedef enum { boldoff = 0, boldon = 1} zoe_bold;
typedef enum { CHOOSE_FILE = 0, CHOOSE_DIR = 1 } zoe_choose;
typedef enum { WITH_LOG = 0, WITHOUT_LOG = 1 } zoe_log;
typedef int objectID;
//--------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct zoe_list_item {
	boolean requested;
	const char *label;
};
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern void		zoe_init( int, int, zoe_log );
extern void		zoe_setattributes( short, zoe_color, zoe_bold );
extern int 		zoe_getnextfreeline( void );
extern void		zoe_title( const char * );
extern void		zoe_text( objectID *, int, int, const char * );
extern int 		zoe_menu( objectID *, int, int, const char **, size_t, const char * );
extern int		zoe_open_question( objectID *, int, int, const char *, const char *, char *, size_t );
extern int		zoe_close_question( objectID *, int, int, const char *, const char **, size_t );
extern int 		zoe_file_chooser( objectID *, int, int, const char *, const char *, zoe_choose, const char **, char * );
extern boolean	zoe_gonogo( objectID *, const char *, const char *, const char * );
extern int 		zoe_list( objectID *, int, int, const char *, struct zoe_list_item *, size_t );
extern void		zoe_progressbar( objectID *, const char * );
extern void		zoe_progressbar_display( objectID, int );
extern void 	zoe_clearObject( objectID );
extern void		zoe_error( const char * );
extern void		zoe_message( const char * );
extern void		zoe_information( objectID *, const char * );
extern void		zoe_end( void );
//------------------------------------------------------------------------------
#endif	// ZOE_H
//==============================================================================
