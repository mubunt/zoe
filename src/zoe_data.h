//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------
#ifndef ZOE_DATA_H
#define ZOE_DATA_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <wchar.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define START_COLOR							"\033[%d;%dm"
#define STOP_COLOR							"\033[0m"

#define REVERSE								"\033[7m"
#define REVERSE_BOLD						"\033[7;1m"
#define GET_CURSOR							"\033[6n"
#define ITALIC								3

#define ANSWER 								"_"
#define	C_H									"─"
#define C_V									"│"

#define	C_ULC								"╭"
#define	C_BLC								"╰"
#define	C_URC								"╮"
#define	C_BRC								"╯"

#define D_H 								"═"
#define D_VHL 								"╡"
#define D_VHR 								"╞"

//#define E_URC								"┐"
//#define E_BRC								"╯"
//#define E_ULC								"┌"
//#define E_BLC								"└"

#define SIMPLELIGHTSHADE					"░"
#define SIMPLEBLOCK							" "

#define SCROLL 								"▒"

#define UPDIR								"../"

#define DISPLAY(...)						fprintf(stdout,  __VA_ARGS__)
#define FLUSH()								fflush(stdout)
#define SAVEINITSCREEN(n)					do { \
												tputs(screeninit, (int)n, putchar); \
												__screeninit = TRUE; \
											} while (0)
#define RESTOREINITSCREEN(n)				if (__screeninit) { \
												SET_TOP_AND_BOTTOM_LINES(1, nbLINES); \
												tputs(screendeinit, (int)n, putchar); \
											}
#define CLEAR_ENTIRE_SCREEN()				tputs(clear, (int)nbLINES, putchar)
#define MOVE_CURSOR(line, column)			tputs(tgoto(move, ((int)(column - 1) % nbCOLS), (int)(line - 1)), 1, putchar)
// To migrate to portable mechanism (terminfo, tputs, ...) later!!!
#define MOVE_CURSOR_AND_CLEAR_LINE(x, y)	DISPLAY("\033[%d;%dH\033[K", x, y)
#define FORMAT_MOVE_CURSOR_AND_CLEAR		"\033[%d;%dH\033[K"
#define HIDE_CURSOR()						DISPLAY("\033[?25l")
#define SHOW_CURSOR()						DISPLAY("\033[?25h")
#define SET_TOP_AND_BOTTOM_LINES(m, n)		DISPLAY("\033[%d;%dr", m, n)
#define DISPLAY_BELL()						do { \
												DISPLAY("\033[?5h"); FLUSH(); \
												usleep(50000); \
												DISPLAY("\033[?5l"); FLUSH(); \
											} while (0)
#define DISPLAY_AT(line, column, ...)		do { \
												MOVE_CURSOR(line, column); \
												fprintf(stdout,  __VA_ARGS__); \
											} while (0)

#define ENTER_ZOE()							do { \
												current_int_signal = signal(SIGINT, SIG_IGN); \
												__sighandler_t current_winch_signal = signal(SIGWINCH, SIG_IGN); \
												if (firstLogLine != -1) { \
													_zoe_getCursorPosition(&__lineinit, &__colinit); \
													SET_TOP_AND_BOTTOM_LINES(firstQuestLine, lastQuestLine); \
												} \
												signal(SIGWINCH, current_winch_signal); \
											} while (0)

#define EXIT_ZOE(free)						do { \
												__sighandler_t current_winch_signal = signal(SIGWINCH, SIG_IGN); \
												_zoe_set_next_free_line(free - 2); \
												if (firstLogLine == -1) { \
													MOVE_CURSOR(free, 1); \
												} else { \
													SET_TOP_AND_BOTTOM_LINES(firstLogLine, nbLINES); \
													MOVE_CURSOR(__lineinit, __colinit); \
												} \
												signal(SIGWINCH, current_winch_signal); \
												signal(SIGINT, current_int_signal); \
												FLUSH(); \
											} while (0)

#define CHECK_BIT(var, pos)					(((var)>>(pos)) & 1)
#define FOREGROUND(color) 					(30 + color)
#define BACKGROUND(color) 					(40 + color)
#define MAX(a,b)							(a > b ? a : b)
#define MIN(a,b)							(a > b ? b : a)

#define _not_known							-10
#define _backspace							-7
#define _abort_session						-6
#define _help								-5
#define _go_next_question					-4
#define _back_previous_question				-3
#define _arrow_up							-2
#define _arrow_down							-1
#define _ok 								0
#define _goon 								1

#define	START_COLUMN_FOR_HELP				(nbCOLS / 8)
#define	END_COLUMN_FOR_HELP					((nbCOLS * 7) / 8)

#define MAXLINE								1024
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { MODE_ECHOED, MODE_RAW }			terminal;
typedef enum { TODISPLAY, TOHIDE }				todo_screenmap;
typedef enum { NOWHERE, DATA_AREA, LOG_AREA} 	cutting_state;
typedef unsigned								_color;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct zoe_attribut {
	_color color;
	zoe_bold bold;
};
struct zoe_attributs {
	struct zoe_attribut title;
	struct zoe_attribut question;
	struct zoe_attribut menu;
	struct zoe_attribut gonogo;
	struct zoe_attribut list;
	struct zoe_attribut chooser;
	struct zoe_attribut text;
	struct zoe_attribut error;
	struct zoe_attribut message;
	struct zoe_attribut information;
	struct zoe_attribut progressbar;
	struct zoe_attribut bar;
	struct zoe_attribut answer;
};

struct screenmap_item {
	objectID objectID;
	int line;
	int column;
	char *label;
	struct screenmap_item *next;
};

struct objectID_item {
	int startline;
	int startcolumn;
	int endline;
	int endcolumn;
	int addtional_information_1;
	int addtional_information_2;
};

struct mapping_close {
	int line;
	int answercolumn;
	int menuline;
	int menucolumn;
	int helpline;
	int helpstartcolumn;
	int helpendcolumn;
};
struct mapping_open {
	int line;
	int answercolumn;
	int answer_additional_line_needed;
	int helpline;
	int helpstartcolumn;
	int helpendcolumn;
};
struct mapping_generic {
	int line;
	int column;
	int helpline;
	int helpstartcolumn;
	int helpendcolumn;
};

struct tree_scanner {
	size_t	nbObjects;
	size_t	maxLenObjects;
	size_t	maxVisibleLenObjects;
	char **ptObjects;
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern int						_allocated_memory;	// Number of allocated memories still to be deleted
extern zoe_log 					_logmode;			// WITH_LOG or WITHOUT_LOG
extern int 						needed_lines;
extern int 						needed_columns;
extern int 						nbLINES;
extern int 						nbCOLS;
extern int 						firstLogLine;
extern int 						firstQuestLine;
extern int 						lastQuestLine;
extern int 						_next_free_line;
extern struct zoe_attributs		attributs;
extern _color					helpcolor;
extern zoe_bold					helpbold;
extern objectID 				objectID_size;
extern struct objectID_item 	*objectID_table;
extern __sighandler_t 			initial_signal;
extern char 					*screeninit;		// Startup terminal initialization
extern char 					*screendeinit;		// Exit terminal de-initialization
extern char 					*clear;				// Clear screen
extern char 					*move;				// Cursor positioning
extern struct screenmap_item 	**screenmap;
extern size_t 					screenmapside;
extern int 						frameNbLines;
extern FILE 					*fdlog;
extern int 						__lineinit;
extern int 						__colinit;
extern boolean					__screeninit;
extern char 					*titleCopy;
extern __sighandler_t 			current_int_signal;
//-- HELP ----------------------------------------------------------------------
extern const char 				help_close_question[];
extern const char 				help_chooser[];
extern const char 				help_gonogo[];
extern const char 				help_information[];
extern const char 				help_list[];
extern const char 				help_menu[];
extern const char 				help_open_question[];
extern const char 				help_title[];
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
extern void						_zoe_fatal( const char *, ... );
extern void						_zoe_warning( const char *, ... );
extern void						_zoe_getTerminalCapabilities( void );
extern void 					_zoe_getTerminalSize( int *, int * );
extern void						_zoe_setTerminalMode( terminal );
extern void 					_zoe_displayLogTitle( void );
extern int 						_zoe_incomingKey( void );
extern int 						_zoe_incomingChar( void );
extern void						_zoe_getCursorPosition( int *, int *);
extern void						_zoe_help( objectID *, int, int, int, int, ... );
extern size_t					_zoe_count_characters_out_escaped_codes( const char * );
extern size_t					_zoe_compute_index_out_escaped_codes( const char *, size_t );
extern int 						_zoe_how_many_lines( const char *, size_t );
extern size_t					_zoe_length_of_longest_line( const char * );
extern void						_zoe_refresh( void );
extern void						_zoe_title( char * );
extern size_t					_zoe_visiblestrlen( const char * );

extern void						_zoe_initObject( void );
extern void						_zoe_endObject( void );
extern objectID					_zoe_newObject( int, int, int, int );
extern void						_zoe_updateObject( objectID, int );
extern void 					_zoe_clearObject( objectID );
extern void 					_zoe_storeInfoObject( objectID, int, int );
extern boolean 					_zoe_retrieveObject( objectID, struct objectID_item * );

extern void 					_zoe_initScreenmap( void );
extern void 					_zoe_reinitScreenmap( size_t );
extern void 					_zoe_endScreenmap( void );
extern void 					_zoe_recordScreenmap( objectID, int, int, char *, todo_screenmap );
extern void						_zoe_removeScreenmap( objectID, int);

extern void						_zoe_map_close( int, int, size_t, size_t, size_t, const char *, struct mapping_close * );
extern void						_zoe_map_chooser( int, int, size_t, size_t *, size_t, const char *, struct mapping_close * );
extern void						_zoe_map_open( int, int, size_t, size_t, const char *, const char *, struct mapping_open * );
extern void						_zoe_map_gonogo( const char *, int, int, const char *, struct mapping_generic * );
extern void						_zoe_map_error( const char *, int, int, const char *, struct mapping_generic * );
extern void						_zoe_map_menu( int, int, size_t, const char *, struct mapping_generic * );
extern void						_zoe_map_list( int, int, size_t, const char *, struct mapping_generic * );

extern void						_zoe_displayTop( objectID, int *, int, size_t, zoe_bold, _color, const char * );
extern void						_zoe_displayBottom( objectID, int, int, size_t, zoe_bold, _color );
extern void						_zoe_displayEmptyLine( objectID, int *, int, size_t, zoe_bold, _color );
extern void						_zoe_displayLabel( objectID, int *, int, size_t, const char *, zoe_bold, _color );
extern void						_zoe_displayOK( objectID, int *, int, size_t, zoe_bold, _color );
extern void						_zoe_displayMenu( objectID, int, int, const char **, size_t, size_t, size_t, size_t, const char *, zoe_bold, _color );

extern struct tree_scanner 		*_zoe_treeScanner( const char *, const char *, zoe_choose, const char **, boolean );
extern void 					_zoe_free_treeScanner( struct tree_scanner * );
extern boolean 					_zoe_isDirectory( char * );
extern boolean 					_zoe_isFile( char * );

extern char 					*_zoe_malloc( size_t, size_t);
extern char 					*_zoe_calloc( size_t, size_t);
extern char 					*_zoe_strdup( const char * );
extern char 					*_zoe_realpath( const char * );
extern void						_zoe_free( void * );

extern void						_zoe_set_next_free_line( int );
//------------------------------------------------------------------------------
#endif	// ZOE_DATA_H
//==============================================================================
