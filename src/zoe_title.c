//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_title( char *title ) {
	char buffer[MAXLINE];
	MOVE_CURSOR_AND_CLEAR_LINE(1, 1);
	MOVE_CURSOR_AND_CLEAR_LINE(2, 1);
	objectID id = _zoe_newObject(1, 1, 2, nbCOLS);
	size_t n = ((size_t) nbCOLS) - strlen(help_title) - 1;
	if (strlen(title) > n) {
		char c = title[n];
		title[n] = '\0';
		sprintf(buffer, START_COLOR "%s" STOP_COLOR, attributs.title.bold, attributs.title.color, title);
		title[n] = c;
	} else
		sprintf(buffer, START_COLOR "%s" STOP_COLOR, attributs.title.bold, attributs.title.color, title);
	_zoe_recordScreenmap(id, 1, 1, buffer, TODISPLAY);
	int col = nbCOLS - (int) strlen(help_title) + 1;
	sprintf(buffer, START_COLOR "%s" STOP_COLOR, ITALIC, attributs.title.color, help_title);
	_zoe_recordScreenmap(id, 1, col, buffer, TODISPLAY);
	*buffer = '\0';
	for (int i = 0; i < nbCOLS; i++) strcat(buffer, C_H);
	_zoe_recordScreenmap(id, 2, 1, buffer, TODISPLAY);
}
//------------------------------------------------------------------------------
// ZOE API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_title( const char *title ) {
	if (title != NULL) {
		ENTER_ZOE();
		if (titleCopy != NULL) _zoe_free(titleCopy);
		titleCopy = _zoe_strdup(title);
		_zoe_title(titleCopy);
		EXIT_ZOE(firstQuestLine);
	}
}
//==============================================================================
