//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static char plouf[58] = "Cannot arrange the window. Please, increase it. Abort!!!";
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_map_close( int line, int column, size_t questionlen,
                     size_t nbvalues, size_t maxwidth, const char *help,
                     struct mapping_close *map ) {
	map->line = firstQuestLine + line - 1;
	map->answercolumn = column + (int) questionlen + 1;
	map->menucolumn = map->answercolumn + (int) maxwidth + 1;
	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	int n = _zoe_how_many_lines(help, width) + 2;
	map->helpline = lastQuestLine - n + 1;
	if (map->helpline <= map->line) {
		map->helpline = 1;
		if (map->line < map->helpline + n - 1)
			_zoe_fatal("%s", plouf);
	}
	map->menuline = map->line - 1;
	if (lastQuestLine < map->menuline + (int) nbvalues + 1) {
		map->menuline = map->line - ((int) nbvalues + 2) / 2;
	}
}
//------------------------------------------------------------------------------
void _zoe_map_chooser( int line, int column, size_t questionlen,
                       size_t *nbvalues, size_t maxwidth, const char *help,
                       struct mapping_close *map ) {
	map->line = firstQuestLine + line - 1;
	map->answercolumn = column + (int) questionlen + 1;
	map->menucolumn = map->answercolumn + 1;
	if (map->menucolumn + (int) maxwidth >  nbCOLS) {
		map->menucolumn = nbCOLS - (int) maxwidth;
		if (map->menucolumn < 0) map->menucolumn = 1;
	}
	size_t n = (size_t) (lastQuestLine - map->line - 6);
	if (n <= 0)
		_zoe_fatal("%s", plouf);
	*nbvalues = MIN(*nbvalues, n);
	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	int m = _zoe_how_many_lines(help, width) + 2;
	map->helpline = lastQuestLine - m + 1;
	if (map->helpline <= map->line) {
		map->helpline = 1;
		if (map->line < map->helpline + m - 1)
			_zoe_fatal("%s", plouf);
	}
	map->menuline = map->line - 1;
	if (lastQuestLine < map->menuline + (int) *nbvalues + 1) {
		map->menuline = map->line - ((int) *nbvalues + 2) / 2;
	}
}
//------------------------------------------------------------------------------
void _zoe_map_open( int line, int column, size_t questionlen, size_t answerlen, const char *help1, const char *help2,
                    struct mapping_open *map ) {
	map->line = firstQuestLine + line - 1;
	map->answercolumn = column + (int) questionlen + 1;

	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	size_t w1 = _zoe_count_characters_out_escaped_codes(help1);
	size_t w2 = _zoe_count_characters_out_escaped_codes(help2);
	if (width < w1 && width < w2)
		width = MAX(w1, w2);
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	int m = nbCOLS - map->answercolumn + 1;
	map->answer_additional_line_needed = ((int) answerlen / m) + (((int) answerlen % m) ? 1 : 0) - 1;
	int n = _zoe_how_many_lines(help1, width) + _zoe_how_many_lines(help2, width) + 2;
	map->helpline = lastQuestLine - n;
	if (map->helpline <= map->line) {
		map->helpline = 1;
		if (map->line < map->helpline + n - 1)
			_zoe_fatal("%s", plouf);
	}
}
//------------------------------------------------------------------------------
void _zoe_map_gonogo( const char *question, int maxwidth, int adding, const char *help, struct mapping_generic *map) {
	map->column = (nbCOLS / 2) - maxwidth / 2;
	int nblines = _zoe_how_many_lines(question, (size_t) maxwidth) + adding;
	map->line = firstQuestLine + ((lastQuestLine - firstQuestLine + 1) / 2) - nblines / 2;
	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	int n = _zoe_how_many_lines(help, width) + 2;
	map->helpline = lastQuestLine - n + 1;
}
//------------------------------------------------------------------------------
void _zoe_map_error( const char *label, int maxwidth, int adding, const char *help, struct mapping_generic *map) {
	map->column = (nbCOLS / 2) - maxwidth / 2;
	int nblines = _zoe_how_many_lines(label, (size_t) maxwidth) + adding;
	int n = lastQuestLine - firstQuestLine + 1;
	if (nblines > n) _zoe_fatal("%s", plouf);
	map->line = firstQuestLine + ((n - nblines) / 2) - 1;
	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	if (help != NULL)
		width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	map->helpline = lastQuestLine;
	if (help != NULL)  {
		n = _zoe_how_many_lines(help, width) + 2;
		map->helpline = map->helpline - n + 1;
	}
}
//------------------------------------------------------------------------------
void _zoe_map_menu( int line, int column, size_t nbvalues, const char *help,
                    struct mapping_generic *map ) {
	map->line = firstQuestLine + line - 1;
	if (lastQuestLine < map->line + (int) nbvalues + 1)
		_zoe_fatal("%s", plouf);
	map->column = column;
	if (help != NULL) {
		size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
		width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
		map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
		map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
		int n = _zoe_how_many_lines(help, width) + 2;
		map->helpline = lastQuestLine - n + 1;
		if (map->helpline <= map->line) {
			map->helpline = 1;
			if (map->line < map->helpline + n - 1)
				_zoe_fatal("%s", plouf);
		}
	}
}
//------------------------------------------------------------------------------
void _zoe_map_list( int line, int column, size_t nitems, const char *help,
                    struct mapping_generic *map) {
	map->line = firstQuestLine + line - 1;
	if (lastQuestLine < map->line + (int) nitems + 7)
		_zoe_fatal("%s", plouf);
	map->column = column;
	size_t width = (size_t) (END_COLUMN_FOR_HELP - START_COLUMN_FOR_HELP - 3);	// +1 - 4 (borders)
	width = MIN(width, _zoe_count_characters_out_escaped_codes(help));
	map->helpstartcolumn = (nbCOLS / 2) - ((int) width + 4) / 2;
	map->helpendcolumn = map->helpstartcolumn + (int) width + 4;
	int n = _zoe_how_many_lines(help, width) + 2;
	map->helpline = lastQuestLine - n + 1;
	if (map->helpline <= map->line) {
		map->helpline = 1;
		if (map->line < map->helpline + n - 1)
			_zoe_fatal("%s", plouf);
	}
}
//==============================================================================
