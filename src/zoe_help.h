//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------
#ifndef ZOE_HELP_H
#define ZOE_HELP_H
//------------------------------------------------------------------------------
const char help_close_question[120] =
    "▼ Next item  / ▲ Previous item / ◀ Come back to previous question / BS, DEL Abort session / CR OK";
const char help_chooser[150] =
    "▼ Next item  / ▲ Previous item / ◀ Come back to previous question / ⯈ Go down to the  directory / BS, DEL Abort session / CR OK";
const char help_gonogo[50] =
    "⯈ Next choice  / ◀ Previous choice / CR OK";
const char help_information[20] =
    "Type RETURN or CR";
const char help_list[110] =
    "▼ Next item  / ▲ Previous item / ⯈ Next choice  / ◀ Previous choice / CR Selection item & OK";
const char help_menu[120] =
    "▼ Next item  / ▲ Previous item / ◀ Come back to previous question / BS, DEL Abort session / CR OK";
const char help_open_question[80] =
    "[◀ Come back to previous question / BS, DEL Abort session / CR OK]";
const char help_title[25] =
    "[For help, type: ctrl A]";
//------------------------------------------------------------------------------
#endif	// ZOE_HELP_H
//==============================================================================
