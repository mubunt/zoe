//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define HIGHLIGHT(n)	sprintf(buffer, START_COLOR "%s %s%*d %-*s " STOP_COLOR START_COLOR "%s" STOP_COLOR, \
    						attributs.question.bold, attributs.question.color, C_V, REVERSE, (int)idxwidth, n + 1, (int)valwidth, values[n], attributs.question.bold, attributs.question.color, C_V); \
						_zoe_recordScreenmap(id2, map.menuline + n + 1, map.menucolumn, buffer, TODISPLAY);
#define UNHIGHLIGHT(n)	sprintf(buffer, START_COLOR "%s %*d %-*s " START_COLOR "%s" STOP_COLOR, \
							attributs.question.bold, attributs.question.color, C_V, (int)idxwidth, n + 1, (int)valwidth, values[n], attributs.question.bold, attributs.question.color, C_V); \
						_zoe_recordScreenmap(id2, map.menuline + n + 1, map.menucolumn, buffer, TODISPLAY);
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
int zoe_close_question( objectID *id, int line, int column,
                        const char *question, const char *values[], size_t nvalues ) {
	struct mapping_close map;
	char buffer[MAXLINE];

	ENTER_ZOE();
	// Size of the longest value
	size_t valwidth = 0;
	for (size_t i = 0; i < nvalues; i++) valwidth = MAX(valwidth, strlen(values[i]));
	// Position of the question / answer / menu / help
	_zoe_map_close(line, column, strlen(question), nvalues, valwidth, (char *)help_close_question, &map);
	// Get object id.
	*id = _zoe_newObject(map.line, column, map.line, column + (int) (strlen(question) + valwidth + 1));
	// Display the question and the area for the answer
	sprintf(buffer, FORMAT_MOVE_CURSOR_AND_CLEAR, map.line, column);
	_zoe_recordScreenmap(*id, map.line, column, buffer, TODISPLAY);
	sprintf(buffer, START_COLOR "%s ", attributs.question.bold, attributs.question.color, question);
	for (size_t i = 0; i < valwidth; i++) strcat(buffer, ANSWER);
	strcat(buffer, STOP_COLOR);
	_zoe_recordScreenmap(*id, map.line, column, buffer, TODISPLAY);
	// Caracteristics of the menu et generation of the associate object
	// The width of the menu is equal to the largest between the widths (valwidth) plus the edges
	// (2 * 2 characters) and the index of values (idxwidth characters + 1)
	size_t idxwidth = (size_t) ((nvalues / 10) + ((nvalues % 10) ? 1 : 0));
	size_t maxwidth = valwidth	+ idxwidth + 5;
	objectID id2 = _zoe_newObject(map.menuline, map.menucolumn, 0, map.menucolumn + (int) maxwidth + 1);
	// Display of the menu
	_zoe_displayMenu(id2, map.menuline, map.menucolumn, values, nvalues, maxwidth, idxwidth, valwidth, (char *) "",attributs.question.bold, attributs.question.color);
	_zoe_updateObject(id2, map.line + frameNbLines - 1);
	// Read selection
	_zoe_setTerminalMode(MODE_RAW);
	int selection = 0;
	int n;
	HIGHLIGHT(selection);
	HIDE_CURSOR();
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	while (1) {
		n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _back_previous_question:
			selection = n = ZOE_BACK;
			break;
		case _abort_session:
			selection = n = ZOE_ABORT;
			break;
		case _arrow_up:
			UNHIGHLIGHT(selection);
			if (selection == 0)
				selection = (int) nvalues - 1;
			else
				--selection;
			HIGHLIGHT(selection);
			n = _goon;
			break;
		case _arrow_down:
			UNHIGHLIGHT(selection);
			if (selection == (int) nvalues - 1)
				selection = 0;
			else
				++selection;
			HIGHLIGHT(selection);
			n = _goon;
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_close_question);
			help_is_displayed = TRUE;
			n = _goon;
			break;
		case _ok:
			n = ZOE_OK;
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			if (n <= (int) nvalues) {
				UNHIGHLIGHT(selection);
				selection = n - 1;
				HIGHLIGHT(selection);
			} else
				DISPLAY_BELL();
			n = _goon;
			break;
		default:
			DISPLAY_BELL();
			n = _goon;
			break;
		}
		if (n == ZOE_OK || n == ZOE_ABORT || n == ZOE_BACK) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
	// Clear menu
	_zoe_clearObject(id2);
	// Display the answer
	if (n == ZOE_OK) {
		sprintf(buffer, START_COLOR "%s" STOP_COLOR, attributs.answer.bold, attributs.answer.color, values[selection]);
		_zoe_recordScreenmap(*id, map.line, map.answercolumn, buffer, TODISPLAY);
	}
	if (n == ZOE_BACK)
		_zoe_clearObject(*id);
	_zoe_refresh();
	EXIT_ZOE(map.line + 1);
	return selection;
}
//==============================================================================
