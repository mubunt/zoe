//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
void _zoe_initScreenmap( void ) {
	screenmap = (struct screenmap_item **) _zoe_malloc(screenmapside + 1, sizeof(struct screenmap_item *));
	for (size_t i = 1; i <= screenmapside; i++) screenmap[i] = NULL;
}
//------------------------------------------------------------------------------
void _zoe_reinitScreenmap( size_t new ) {
	if (NULL == (screenmap = realloc(screenmap, sizeof(struct screenmap_item *) * (screenmapside + 1))))
		_zoe_fatal("%s", "Cannot reallocate memory for screen map. Abort!");
	for (size_t i = new; i <= screenmapside; i++) screenmap[i] = NULL;
}
//------------------------------------------------------------------------------
void _zoe_endScreenmap( void ) {
	for (size_t i = 1; i <= screenmapside; i++) {
		if (screenmap[i] != NULL) {
			struct screenmap_item *pt = screenmap[i];
			while (pt != NULL) {
				struct screenmap_item *next = pt->next;
				_zoe_free(pt->label);
				_zoe_free(pt);
				pt = next;
			}
			screenmap[i] = NULL;
		}
	}
	_zoe_free(screenmap);
}
//------------------------------------------------------------------------------
void _zoe_recordScreenmap( objectID id, int line, int column, char *label, todo_screenmap todo ) {
	if (line <= 0 || line > (int) screenmapside) {
		_zoe_warning("WARNING: Required line (%d) outside the number of accessible lines (1 to %d).",
		             line - firstQuestLine + 1, lastQuestLine - firstQuestLine + 1);
		return;
	}
	struct screenmap_item *pt = screenmap[line];
	while (pt != NULL) {
		if (pt->column == column) {
			struct screenmap_item *next = pt->next;
			_zoe_removeScreenmap(pt->objectID, line);
			pt = next;
		} else
			pt = pt->next;

	}
	struct screenmap_item *newlabel = (struct screenmap_item *) _zoe_malloc(1, sizeof(struct screenmap_item));
	newlabel->objectID = id;
	newlabel->line = line;
	newlabel->column = column;
	newlabel->label = _zoe_strdup(label);
	newlabel->next = NULL;
	if (screenmap[line] == NULL)
		screenmap[line] = newlabel;
	else {
		struct screenmap_item *pt = screenmap[line];
		while (pt->next != NULL) pt = pt->next;
		pt->next = newlabel;
	}
	if (todo == TODISPLAY)
		DISPLAY_AT(line, column, "%s", label);
}
//------------------------------------------------------------------------------
void _zoe_removeScreenmap( objectID id, int line) {
	if (line <= 0 || line > (int) screenmapside) return;
	if (screenmap[line] == NULL) return;
	struct screenmap_item *pt = screenmap[line];
	struct screenmap_item *previous = NULL;
	while (pt != NULL) {
		if (pt->objectID == id) {
			struct screenmap_item *next = pt->next;
			_zoe_free(pt->label);
			_zoe_free(pt);
			pt = next;
			if (previous == NULL)
				screenmap[line] = pt;
			else
				previous->next = pt;
		} else {
			previous = pt;
			pt = pt->next;
		}
	}
}
//------------------------------------------------------------------------------
void _zoe_refresh( void ) {
	for (size_t i = 1; i <= screenmapside; i++) {
		if (screenmap[i] != NULL) {
			struct screenmap_item *pt = screenmap[i];
			while (pt != NULL) {
				DISPLAY_AT(pt->line, pt->column, "%s", pt->label);
				pt = pt->next;
			}
		}
	}
	FLUSH();
}
//==============================================================================
