//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
void zoe_text( objectID *id, int line, int column, const char *text ) {
	char buffer[MAXLINE];
	ENTER_ZOE();
	int trueline = firstQuestLine + line - 1;
	*id = _zoe_newObject(trueline, column, trueline, column + (int) strlen(text));
	sprintf(buffer, START_COLOR "%s" STOP_COLOR, attributs.text.bold, attributs.text.color, text);
	_zoe_recordScreenmap(*id, trueline, column, buffer, TODISPLAY);
	EXIT_ZOE(firstQuestLine + line);
}
//==============================================================================
