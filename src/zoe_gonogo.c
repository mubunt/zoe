//------------------------------------------------------------------------------
// Copyright (c) 2018-2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: libZoe, a library providing an API for developing simple text-based environments.
//
// zoe is is a programming library providing a simple application programming
// interface (API) that allows the programmer to write text-based user interfaces
// in a terminal-independent manner.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe_data.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static char gohighlighted[MAXLINE];
static char nogohighlighted[MAXLINE];
static int linehighlighted;
static int columnhighlighted;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define INITFIELD(field, a)		do { \
									size_t p = (bwidth - strlen(a) - 2) / 2;  \
									strcpy(field, " "); \
									for (size_t i = 0; i < p; i++) strcat(field, " "); \
									strcat(field, a); \
									for (size_t i = 0; i < bwidth - strlen(a) - p - 2; i++) strcat(field, " "); \
									strcat(field, " "); \
								} while (0);
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void displaychoice( objectID id, int *line, int column, size_t width, size_t bwidth,
                           const char *go, const char *nogo ) {
	char buf[16];
	sprintf(buf, STOP_COLOR START_COLOR, attributs.gonogo.bold, attributs.gonogo.color);
	linehighlighted = *line;
	columnhighlighted = column;
	char *field1 = _zoe_malloc(bwidth + 1, sizeof(char));
	char *field2 = _zoe_malloc(bwidth + 1, sizeof(char));
	INITFIELD(field1, go);
	INITFIELD(field2, nogo);
	int p = (int) ((width - 2) - (2 * bwidth));

	sprintf(nogohighlighted, START_COLOR "%s", attributs.gonogo.bold, attributs.gonogo.color, C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, REVERSE);
	strcat(nogohighlighted, field2);
	strcat(nogohighlighted, buf);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, field1);
	for (int i = 0; i < p / 3 ; i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, C_V);
	strcat(nogohighlighted, STOP_COLOR);

	sprintf(gohighlighted, START_COLOR "%s", attributs.gonogo.bold, attributs.gonogo.color, C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, field2);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, REVERSE);
	strcat(gohighlighted, field1);
	strcat(gohighlighted, buf);
	for (int i = 0; i < p / 3 ; i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, C_V);
	strcat(gohighlighted, STOP_COLOR);

	_zoe_free(field1);
	_zoe_free(field2);
	++frameNbLines;
	_zoe_recordScreenmap(id, (*line)++, column, nogohighlighted, TODISPLAY);
}
//------------------------------------------------------------------------------
// ZOP API FUNCTIONS
//------------------------------------------------------------------------------
boolean zoe_gonogo( objectID *id, const char *question, const char *go, const char *nogo ) {
	struct mapping_generic map;
	ENTER_ZOE();
	// The width of the menu is the largest between the widths of,
	// the question, and the button line, plus the edges (2 * 2 characters)
	size_t buttonwidth = 4 + MAX(strlen(go), strlen(nogo));
	size_t width =  4 + MAX(strlen(question), 2 * buttonwidth + 2);

	// Position of the question / answer / menu / help. 6 = frame + empty line + buttons
	_zoe_map_gonogo(question, (int) width, 6, (char *)help_gonogo, &map);
	// Get object id.
	*id = _zoe_newObject(map.line, map.column, 0, map.column + (int) width + 1);
	// Display frame including question and choice buttons
	int line = map.line;
	_zoe_displayTop(*id, &line, map.column, width, attributs.gonogo.bold, attributs.gonogo.color, (char *) "");
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.gonogo.bold, attributs.gonogo.color);
	_zoe_displayLabel(*id, &line, map.column, width - 4, question, attributs.gonogo.bold, attributs.gonogo.color);
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.gonogo.bold, attributs.gonogo.color);
	displaychoice(*id, &line, map.column, width, buttonwidth, go, nogo);
	_zoe_displayEmptyLine(*id, &line, map.column, width, attributs.gonogo.bold, attributs.gonogo.color);
	_zoe_displayBottom(*id, line, map.column, width, attributs.gonogo.bold, attributs.gonogo.color);
	FLUSH();
	_zoe_updateObject(*id, map.line + frameNbLines - 1);
	// Read selection
	_zoe_setTerminalMode(MODE_RAW);
	boolean selection = FALSE;
	boolean help_is_displayed = FALSE;
	objectID idhelp;
	HIDE_CURSOR();
	while (1) {
		int n = _zoe_incomingKey();
		if (help_is_displayed) {
			_zoe_clearObject(idhelp);
			help_is_displayed = FALSE;
		}
		switch (n) {
		case _go_next_question:
			if (selection)
				DISPLAY_BELL();
			else {
				selection = TRUE;
				DISPLAY_AT(linehighlighted, columnhighlighted, "%s", gohighlighted);
			}
			break;
		case _back_previous_question:
			if (selection) {
				selection = FALSE;
				DISPLAY_AT(linehighlighted, columnhighlighted, "%s", nogohighlighted);
			} else
				DISPLAY_BELL();
			break;
		case _help:
			_zoe_help(&idhelp, map.helpline, map.helpstartcolumn, map.helpendcolumn, 1, (char *) help_gonogo);
			help_is_displayed = TRUE;
			break;
		case _ok:
			n = ZOE_OK;
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ZOE_OK) break;
	}
	SHOW_CURSOR();
	_zoe_setTerminalMode(MODE_ECHOED);
	_zoe_clearObject(*id);
	_zoe_refresh();
	EXIT_ZOE(map.line + frameNbLines);
	return selection;
}
//==============================================================================
